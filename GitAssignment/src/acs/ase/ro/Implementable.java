package acs.ase.ro;

public interface Implementable {
	public boolean check(Document newVersion, Document oldVersion);
}
