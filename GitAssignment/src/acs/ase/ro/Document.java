package acs.ase.ro;

public class Document implements Implementable {

	private String paragraph;
	private float paragraphVersion;
	public Document(String paragraph, float paragraphVersion) {
		super();
		this.paragraph = paragraph;
		this.paragraphVersion = paragraphVersion;
	}
	@Override
	public boolean check(Document newVersion, Document oldVersion) {
		if(newVersion.paragraphVersion>oldVersion.paragraphVersion) return true;
		else
		return false;
	}
	
	
}
